package com.coolrocket.mediacodectest

enum class CaptureSessionEvents {

    ON_STARTED,
    ON_PROGRESSED,
    ON_COMPLETED,
    ON_SEQUENCE_COMPLETED,
    ON_SEQUENCE_ABORTED

}
