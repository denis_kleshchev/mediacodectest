package com.coolrocket.mediacodectest

import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CaptureRequest
import android.hardware.camera2.CaptureResult

data class CaptureSessionData(
    val event: CaptureSessionEvents,
    val session: CameraCaptureSession,
    val request: CaptureRequest,
    val result: CaptureResult
)
