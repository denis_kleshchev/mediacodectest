package com.coolrocket.mediacodectest

import android.media.MediaCodec
import java.nio.ByteBuffer

sealed class EncodedWrapper {

    data class Successful(
        val data: ByteBuffer,
        val info: MediaCodec.BufferInfo
    ) : EncodedWrapper()

    object Empty : EncodedWrapper()
}