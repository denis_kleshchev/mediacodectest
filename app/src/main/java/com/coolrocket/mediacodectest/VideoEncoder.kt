package com.coolrocket.mediacodectest

import android.media.MediaCodec
import android.media.MediaCodecInfo
import android.media.MediaFormat
import android.media.MediaMuxer
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.util.Log
import android.view.Surface
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class VideoEncoder(
    width: Int,
    height: Int,
    bitRate: Int,
    frameRate: Int
) {

    companion object {
        private const val VIDEO_MIME_TYPE = "video/avc"
        private const val IFRAME_INTERVAL = 1
    }

    private val videoFormat = MediaFormat.createVideoFormat(VIDEO_MIME_TYPE, width, height).apply {
        setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface)
        setInteger(MediaFormat.KEY_BIT_RATE, bitRate)
        setInteger(MediaFormat.KEY_FRAME_RATE, frameRate)
        setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL)
    }

    lateinit var inputSurface: Surface

    private val encoder: MediaCodec = MediaCodec.createEncoderByType(VIDEO_MIME_TYPE)

    private val trigger by lazy {  Observable.interval(3L, TimeUnit.SECONDS) }

    private fun configureEncoder() {
        encoder.apply {
            configure(videoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
            setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
            inputSurface = createInputSurface()
        }
    }

    private fun codecObservable() = callbackObservable(encoder)
        .share()

    private fun outputBuffers(): Observable<MediaCodecCallbackEvents.OutputBufferAvailable> = codecObservable()
        .doOnNext { Log.d("encoder", "codecObservable: $it") }
        .filter { it is MediaCodecCallbackEvents.OutputBufferAvailable }
        .map { it as MediaCodecCallbackEvents.OutputBufferAvailable }
        .doOnNext { Log.d("encoder", "codecObservable after filter: $it") }
        .share()

    private fun inputBuffers(): Observable<MediaCodecCallbackEvents.InputBufferAvailable> = codecObservable()
        .filter { it is MediaCodecCallbackEvents.InputBufferAvailable }
        .map { it as MediaCodecCallbackEvents.InputBufferAvailable }
        .share()

    private var subscription: Disposable? = null

    fun listen() {
        subscription = outputBuffers()
            .map { handleOutputBufferAvailable(it) }
            .filter { it is EncodedWrapper.Successful }
            .map { it as EncodedWrapper.Successful }
            .buffer(trigger)
            .flatMap { wrappers ->
                handleBuffer(wrappers)
                Observable.empty<Any>()
            }
            .subscribe(
                { Log.d("encoder", "ok listening $it") },
                {
                    Log.e("encoder", "listening error:\n$it")
                }
            )
    }

    fun release() {
        subscription.safeDispose()
    }

    private fun handleBuffer(wrappers: List<EncodedWrapper.Successful>) {
        getCaptureFile(Environment.DIRECTORY_MOVIES, ".mp4")?.let {
            val mediaMuxer = MediaMuxer(it.absolutePath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)
            val trackIndex = mediaMuxer.addTrack(encoder.outputFormat)
            mediaMuxer.start()
            for (wrapper in wrappers) {
                mediaMuxer.writeSampleData(trackIndex, wrapper.data, wrapper.info)
            }
            mediaMuxer.stop()
            mediaMuxer.release()
        }
    }

    private fun callbackObservable(mediaCodec: MediaCodec): Observable<MediaCodecCallbackEvents> =
        Observable.create { emitter ->
            mediaCodec.setCallback(object : MediaCodec.Callback() {
                override fun onOutputBufferAvailable(p0: MediaCodec, p1: Int, p2: MediaCodec.BufferInfo) {
                    Log.d("encoder", "onOutputBufferAvailable index $p1")
                    emitter.onNext(MediaCodecCallbackEvents.OutputBufferAvailable(p0, p1, p2))
                }

                override fun onInputBufferAvailable(p0: MediaCodec, p1: Int) {
                    Log.d("encoder", "onInputBufferAvailable index $p1")
                    emitter.onNext(MediaCodecCallbackEvents.InputBufferAvailable(p0, p1))
                }

                override fun onOutputFormatChanged(p0: MediaCodec, p1: MediaFormat) {
                    Log.d("encoder", "onOutputFormatChanged format $p1")
                    emitter.onNext(MediaCodecCallbackEvents.OutputFormatChanged(p0, p1))
                }

                override fun onError(p0: MediaCodec, p1: MediaCodec.CodecException) {
                    Log.d("encoder", "onError $p1")
                    emitter.onError(p1)
                }
            })
            configureEncoder()
            mediaCodec.start()
        }

    private fun handleOutputBufferAvailable(it: MediaCodecCallbackEvents.OutputBufferAvailable): EncodedWrapper {
        if ((it.info.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
            it.info.size = 0
        }

        return if (it.info.size != 0) {

            val buffer = it.codec.getOutputBuffer(it.index)?.apply {
                position(it.info.offset)
                limit(it.info.offset + it.info.size)
            }
            it.codec.releaseOutputBuffer(it.index, false)

            buffer
                ?.let { notEmptyBuffer -> EncodedWrapper.Successful(notEmptyBuffer, it.info) }
                ?: EncodedWrapper.Empty
        } else {
            EncodedWrapper.Empty
        }
    }

    private fun getCaptureFile(type: String, ext: String): File? {
        val dir = File(getExternalStoragePublicDirectory(type), "GRAFIKA")
        dir.mkdirs()
        return if (dir.canWrite()) {
            File(dir, "$getDateTimeString$ext")
        } else null
    }


    private val getDateTimeString: String
        get () = GregorianCalendar()
            .run { dateTimeFormat.format(time) }


    private val dateTimeFormat = SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US)


}
