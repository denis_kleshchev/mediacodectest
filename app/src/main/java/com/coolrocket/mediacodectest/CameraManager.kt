package com.coolrocket.mediacodectest

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.camera2.*
import android.hardware.camera2.CameraManager
import android.view.Surface
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.disposables.Disposable

class CameraManager(
    context: Context
) {

    private val manager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    private lateinit var cameraParams: CameraCharacteristics
    private var previewDisposable: Disposable? = null

    lateinit var previewSurface: Surface
    var recordSurface: Surface? = null

    fun subscribePreview() {
        previewDisposable.safeDispose()
        previewDisposable = openCamera(frontCameraId)
            .filter { it.first == CameraDeviceStateEvents.ON_OPENED }
            .map { it.second }
            .flatMap { device ->
                createCaptureSession(device,
                    recordSurface?.let {
                        listOf(previewSurface, it)
                    } ?: listOf(previewSurface)
                )
            }
            .filter { it.first == CaptureSessionStateEvents.ON_CONFIGURED }
            .map { it.second }
            .flatMap { session ->
                val recordObservable = recordSurface?.let {
                    fromSetRepeatingRequest(session, createPreviewBuilder(session, it).build())
                } ?: Observable.empty()

                val builder = createPreviewBuilder(session, previewSurface)
                fromSetRepeatingRequest(session, builder.build())
                    .mergeWith(recordObservable)
            }
            .subscribe()
    }

    fun releasePreview() {
        previewDisposable.safeDispose()
    }

    @SuppressLint("MissingPermission")
    private fun openCamera(cameraId: String): Observable<Pair<CameraDeviceStateEvents, CameraDevice>> {
        return Observable.create { emitter ->
            manager.openCamera(cameraId, object : CameraDevice.StateCallback() {
                override fun onOpened(camera: CameraDevice) {
                    emitter.onNext(Pair(CameraDeviceStateEvents.ON_OPENED, camera))
                }

                override fun onDisconnected(camera: CameraDevice) {
                    emitter.onNext(Pair(CameraDeviceStateEvents.ON_DISCONNECTED, camera))
                }

                override fun onError(camera: CameraDevice, error: Int) {
                    emitter.onError(OpenCameraException(error))
                }

                override fun onClosed(camera: CameraDevice) {
                    emitter.onNext(Pair(CameraDeviceStateEvents.ON_CLOSED, camera))
                }
            }, null)
        }
    }

    private val frontCameraId
        get() = manager.cameraIdList
            .first {
                manager.getCameraCharacteristics(it)
                    .get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT
            }
            .also { cameraParams = manager.getCameraCharacteristics(it) }

    private fun createCaptureSession(
        cameraDevice: CameraDevice,
        surfaces: List<Surface>
    ): Observable<Pair<CaptureSessionStateEvents, CameraCaptureSession>> {
        return Observable.create { emitter ->
            cameraDevice.createCaptureSession(surfaces, object : CameraCaptureSession.StateCallback() {

                override fun onConfigureFailed(session: CameraCaptureSession) {
                    emitter.onError(CreateCaptureSessionException(session))
                }

                override fun onConfigured(session: CameraCaptureSession) {
                    emitter.onNext(Pair(CaptureSessionStateEvents.ON_CONFIGURED, session))
                }

                override fun onReady(session: CameraCaptureSession) {
                    emitter.onNext(Pair(CaptureSessionStateEvents.ON_READY, session))
                }

                override fun onCaptureQueueEmpty(session: CameraCaptureSession) {
                    emitter.onNext(Pair(CaptureSessionStateEvents.ON_CAPTURE_QUEUE_EMPTY, session))
                }

                override fun onClosed(session: CameraCaptureSession) {
                    emitter.onNext(Pair(CaptureSessionStateEvents.ON_CLOSED, session))
                }

                override fun onSurfacePrepared(session: CameraCaptureSession, surface: Surface) {
                    emitter.onNext(Pair(CaptureSessionStateEvents.ON_SURFACE_PREPARED, session))
                }

                override fun onActive(session: CameraCaptureSession) {
                    emitter.onNext(Pair(CaptureSessionStateEvents.ON_ACTIVE, session))
                }
            }, null)
        }
    }

    private fun createPreviewBuilder(
        captureSession: CameraCaptureSession,
        previewSurface: Surface?
    ): CaptureRequest.Builder {
        return captureSession.device.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW).apply {
            previewSurface?.let { addTarget(it) }
            setup3Auto(this)
        }
    }

    private fun setup3Auto(builder: CaptureRequest.Builder) {
        builder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO)

        val minFocusDistance: Float? = cameraParams.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE)

        val noAutoFocusRun = minFocusDistance?.let { it == 0f } ?: true

        if (!noAutoFocusRun) {
            val autoFocusModes = cameraParams.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES)
            if (autoFocusModes != null && autoFocusModes.contains(CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)) {
                builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)
            } else {
                builder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO)
            }
        }

        val aeModes = cameraParams.get(CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES)
        if (aeModes != null && aeModes.contains(CaptureRequest.CONTROL_AWB_MODE_AUTO)) {
            builder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO)
        }

    }

    private fun fromSetRepeatingRequest(
        captureSession: CameraCaptureSession,
        request: CaptureRequest
    ): Observable<CaptureSessionData> = Observable.create { emitter ->
        captureSession.setRepeatingRequest(request, createCaptureCallback(emitter), null)
    }

    private fun createCaptureCallback(
        emitter: ObservableEmitter<CaptureSessionData>
    ): CameraCaptureSession.CaptureCallback {

        fun doIfNotDisposed(action: (ObservableEmitter<CaptureSessionData>) -> Unit) {
            if (!emitter.isDisposed) {
                action(emitter)
            }
        }

        fun nextIfNotDisposed(action: CaptureSessionData) {
            doIfNotDisposed {
                it.onNext(action)
            }
        }

        fun errorIfNotDisposed(action: Throwable) {
            doIfNotDisposed {
                it.onError(action)
            }
        }

        return object : CameraCaptureSession.CaptureCallback() {

            override fun onCaptureCompleted(
                session: CameraCaptureSession,
                request: CaptureRequest,
                result: TotalCaptureResult
            ) {
                nextIfNotDisposed(
                    CaptureSessionData(
                        CaptureSessionEvents.ON_COMPLETED,
                        session,
                        request,
                        result
                    )
                )
            }

            override fun onCaptureFailed(
                session: CameraCaptureSession,
                request: CaptureRequest,
                failure: CaptureFailure
            ) {
                errorIfNotDisposed(CameraCaptureFailedException(failure))
            }

        }
    }

}
