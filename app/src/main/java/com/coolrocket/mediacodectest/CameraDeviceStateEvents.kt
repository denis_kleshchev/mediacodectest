package com.coolrocket.mediacodectest

enum class CameraDeviceStateEvents {

    ON_OPENED,
    ON_CLOSED,
    ON_DISCONNECTED

}
