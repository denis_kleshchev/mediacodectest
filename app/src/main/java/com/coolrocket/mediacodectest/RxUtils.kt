package com.coolrocket.mediacodectest

import io.reactivex.disposables.Disposable

fun Disposable?.safeDispose() {
    if (this != null && !this.isDisposed) {
        dispose()
    }
}
