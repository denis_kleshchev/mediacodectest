package com.coolrocket.mediacodectest

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.SurfaceHolder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private val manager: CameraManager by lazy {
        CameraManager(applicationContext)
    }

    private var encoder: VideoEncoder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        surf.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
                p0?.surface?.let {
                    manager.previewSurface = it
//                    encoder = VideoEncoder(1920, 1080, 6000000, 60)
//                        .apply {
//                            listen()
//                            manager.recordSurface = inputSurface
//                        }
                    manager.subscribePreview()
                }
            }

            override fun surfaceDestroyed(p0: SurfaceHolder?) {
                manager.releasePreview()
                encoder?.release()
                encoder = null
            }

            override fun surfaceCreated(p0: SurfaceHolder?) {
                p0?.surface?.let {
                    manager.previewSurface = it
//                    encoder = VideoEncoder(1920, 1080, 6000000, 60)
//                        .apply {
//                            listen()
//                            manager.recordSurface = inputSurface
//                        }
                    manager.subscribePreview()
                }
            }
        })
    }

}
