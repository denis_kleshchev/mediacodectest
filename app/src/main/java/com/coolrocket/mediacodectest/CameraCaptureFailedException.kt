package com.coolrocket.mediacodectest

import android.hardware.camera2.CaptureFailure

class CameraCaptureFailedException(val failure: CaptureFailure) : RuntimeException()
