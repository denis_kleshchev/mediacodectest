package com.coolrocket.mediacodectest

import android.media.MediaCodec
import android.media.MediaFormat

sealed class MediaCodecCallbackEvents(val codec: MediaCodec) {

    class OutputBufferAvailable(
        codec: MediaCodec,
        val index: Int,
        val info: MediaCodec.BufferInfo
    ) : MediaCodecCallbackEvents(codec)

    class InputBufferAvailable(
        codec: MediaCodec,
        val index: Int
    ) : MediaCodecCallbackEvents(codec)

    class OutputFormatChanged(
        codec: MediaCodec,
        format: MediaFormat
    ) : MediaCodecCallbackEvents(codec)

}
