package com.coolrocket.mediacodectest

enum class CaptureSessionStateEvents {

    ON_CONFIGURED,
    ON_READY,
    ON_ACTIVE,
    ON_CLOSED,
    ON_SURFACE_PREPARED,
    ON_CAPTURE_QUEUE_EMPTY

}
