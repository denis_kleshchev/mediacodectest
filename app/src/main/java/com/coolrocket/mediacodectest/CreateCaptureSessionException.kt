package com.coolrocket.mediacodectest

import android.hardware.camera2.CameraCaptureSession

class CreateCaptureSessionException(val session: CameraCaptureSession) : RuntimeException()
