package com.coolrocket.mediacodectest

class OpenCameraException(val reason: Int) : RuntimeException()
